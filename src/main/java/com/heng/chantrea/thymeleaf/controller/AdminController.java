package com.heng.chantrea.thymeleaf.controller;

import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @GetMapping("/login")
    public String viewLoginPage(){
        return "admin/login-component/login";
    }

    @GetMapping("/logout")
    public String viewLogoutPage(){
        return "admin/logout-component/logout";
    }

}
