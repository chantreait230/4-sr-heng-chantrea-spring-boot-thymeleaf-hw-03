package com.heng.chantrea.thymeleaf.controller;

import com.heng.chantrea.thymeleaf.annotation.IsAdmin;
import com.heng.chantrea.thymeleaf.annotation.IsEditor;
import com.heng.chantrea.thymeleaf.annotation.IsReviewer;
import com.heng.chantrea.thymeleaf.annotation.IsUser;
import com.heng.chantrea.thymeleaf.model.Article;
import com.heng.chantrea.thymeleaf.repository.ArticleRepository;
import org.apache.catalina.Role;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.List;

@IsUser
@Controller
@RequestMapping("/admin")
public class ArticleController {

    private ArticleRepository articleRepository;

    @Autowired
    public ArticleController(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @GetMapping("/dashboard")
    @IsAdmin
    public String viewDashboardPage(){
        return "admin/layouts/dashboard";
    }

    @PostMapping("/article")
    @IsEditor
    public String addArticle(@ModelAttribute Article article, HttpServletRequest request){
        articleRepository.save(article);
        String route = "";
        if(request.isUserInRole("ROLE_EDITOR")){
            route = "/admin/article";
        }else if(request.isUserInRole("ROLE_ADMIN")){
            route = "redirect:/admin/review-articles";
        }
        return route;
    }

    @GetMapping("/review-articles")
    @IsReviewer
    public String viewArticleListPage(ModelMap modelMap){
        List<Article> articles = (List<Article>) articleRepository.findAll();
        modelMap.addAttribute("articles",articles);
        return "article/article-table/article-table";
    }

    @GetMapping("/article")
    @IsEditor
    public String viewArticleFormPage(@ModelAttribute Article article, ModelMap modelMap){
        modelMap.addAttribute("article",article);
        return "article/article-form/article-form";
    }

    @GetMapping("/home")
    public String homepage() {
        return "admin/home/home";
    }
}
