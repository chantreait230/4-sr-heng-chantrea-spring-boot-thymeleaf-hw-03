package com.heng.chantrea.thymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {

    @GetMapping("/404")
    public String handle404() {
        return "error/404";
    }

    @GetMapping("/403")
    public String handle403() {
        return "error/403";
    }

    @GetMapping("/500")
    public String handle500() {
        return "error/500";
    }
}
