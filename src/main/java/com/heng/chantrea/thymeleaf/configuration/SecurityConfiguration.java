package com.heng.chantrea.thymeleaf.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final String SECRET_KEY = "123456";

    @Autowired
    private SimpleAuthenticationSuccessHandler successHandler;

    @Autowired
    private CustomUserDetailService customUserDetailService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.formLogin()
                .loginPage("/admin/login")
                .failureUrl("/admin/login?error")
                .successHandler(successHandler())
                .and()
                .logout()
                .logoutUrl("/admin/logout")
                .logoutSuccessUrl("/admin/dashboard")
                .and()
                .rememberMe()
                .rememberMeServices(getRememberMeServices())
                .key(SECRET_KEY)
                .and()
                .authorizeRequests().anyRequest().permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/403");
    }

    private TokenBasedRememberMeServices getRememberMeServices() {

        TokenBasedRememberMeServices services = new TokenBasedRememberMeServices(SECRET_KEY, customUserDetailService);
        services.setCookieName("remember-cookie");
        services.setTokenValiditySeconds(100); // Default 14 days
        return services;
    }

    @Bean
    public SimpleAuthenticationSuccessHandler successHandler() {
        return new SimpleAuthenticationSuccessHandler();
    }

}
