package com.heng.chantrea.thymeleaf.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class UserMemory {


    private Map<String, CustomUser> data;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public UserMemory() {

        data = new HashMap<>();

        CustomUser dara = new CustomUser(1, "dara", getPassword("dara123"), getGrants("ROLE_USER"));
        CustomUser kanha = new CustomUser(2, "kanha", getPassword("kanha123"), getGrants("ROLE_EDITOR"));
        CustomUser reksmey = new CustomUser(3, "reksmey", getPassword("reksmey123"), getGrants("ROLE_REVIEWER"));
        CustomUser makara = new CustomUser(4, "makara", getPassword("makara123"), getGrants("ROLE_ADMIN"));

        data.put("dara", dara);
        data.put("kanha", kanha);
        data.put("reksmey", reksmey);
        data.put("makara", makara);
    }

    public Map<String, CustomUser> getUserMemory() {
        return data;
    }

    private String getPassword(String raw) {
        return passwordEncoder.encode(raw);
    }

    private Collection<GrantedAuthority> getGrants(String role) {

        return AuthorityUtils.commaSeparatedStringToAuthorityList(role);

    }
}
