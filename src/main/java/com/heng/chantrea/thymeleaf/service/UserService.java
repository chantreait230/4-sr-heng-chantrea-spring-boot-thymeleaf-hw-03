package com.heng.chantrea.thymeleaf.service;

import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserMemory userMemory = new UserMemory();

    public CustomUser getUserByUsername(String username) {
        CustomUser originUser = userMemory.getUserMemory().get(username);
        if (originUser == null) {
            return null;
        }
        return new CustomUser(originUser.getId(), originUser.getUsername(), originUser.getPassword(), originUser.getAuthorities());
    }
}
